

interface ResultImpl<T,E> {
  /**
   * Transform the value using a given function, if the Result holds a value.
   * @param f The function to apply to the value.
   * @returns A new Result object.
   */
  map<T2>(f: (val: T) => T2): Result<T2,E>;

  /**
   * Transform the error using a given function, if the Result holds an error.
   * @param f The function to apply to the error.
   * @returns A new Result object.
   */
  mapError<E2>(f: (error: E) => E2): Result<T,E2>;

  /**
   * Transform the Result using one of the given functions depending on whether
   * the Result holds a value or an error.
   * @param f1 The function to apply if the Result holds a value.
   * @param f2 The function to apply if the Result holds an error.
   * @returns A new Result object.
   */
  mapBoth<T2,E2>(f1: (val: T) => T2, f2: (error: E) => E2): Result<T2,E2>;

  /**
   * Chain together multiple functions that return a Result.
   * @param f1 The function to apply if the Result holds a value.
   * @param f2 An optional function to apply if the Result holds an error.
   * @returns A new Result object.
   */
  then<T2>(f1: (val: T) => Result<T2,E>, f2?: (error: E) => Result<T2,E>): Result<T2,E>;

  /**
   * Recover from a potential error. Allows partial recovery by mapping error
   * values to either an Ok value or an Err that may optionally hold a different
   * error type.
   * @param f The function to apply if the Result holds an error.
   * @returns A new Result object.
   */
  recover<E2>(f: (error: E) => Result<T,E2>): Result<T,E2>;

  /**
   * Attempts to transform the value using the given function, if the Result
   * holds a value. If an error is thrown, the provided handler catches it and
   * transforms into the appropriate error type.
   * @param f A function that may throw.
   * @param handleError A function to handle the error if `f` throws.
   * @returns A new Result object.
   */
  try<T2>(f: (val: T) => T2, handleError: (e: unknown) => E): Result<T2,E>;

  /**
   * Get the value from the Result if it holds one, or a default value it not.
   * @param val The default value to use if the Result holds an error.
   */
  withDefault(val: T): T;

  /**
   * Get the value from the Result if it holds one, or transform the error into
   * a value if not.
   * @param f The function to apply if the Result holds an error.
   */
  catch(f: (error: E) => T): T;

  /**
   * Transform the Result into a Promise that either resolves to the value if it
   * holds one, or rejects with the error as the reason otherwise.
   */
  asPromise(): Promise<T>;

}

class Ok<T,E> implements ResultImpl<T,E> {

  public readonly ok: true = true;

  /**
   * @param value The underlying value held by the Result.
   */
  constructor(public readonly value: T) {}

  map<T2>(f: (val: T) => T2): Ok<T2,E> {
    return ok(f(this.value));
  }

  mapError<E2>(f: (error: E) => E2): Ok<T,E2> {
    return ok(this.value);
  }

  mapBoth<T2, E2>(f1: (val: T) => T2, f2: (error: E) => E2): Ok<T2, E2> {
    return ok(f1(this.value));
  }

  then<T2>(f1: (val: T) => Result<T2,E>, f2?: (error: E) => Result<T2,E>): Result<T2,E> {
    return f1(this.value);
  }

  try<T2>(f: (val: T) => T2, handleError: (e: unknown) => E): Result<T2,E> {
    return this.then(lift(f, handleError));
  }

  recover<E2>(f: (error: E) => Result<T,E2>): Ok<T,E2> {
    return ok(this.value);
  }

  withDefault(val: T): T {
    return this.value;
  }

  catch(f: (error: E) => T): T {
    return this.value;
  }

  asPromise(): Promise<T> {
    return Promise.resolve(this.value);
  }

}

/**
 * Create a Result from the given value.
 * @param val The value to hold in the Result.
 * @returns A new Result object.
 */
export function ok<T>(val: T): Ok<T,any> {
  return new Ok(val);
}

class Err<T,E> implements ResultImpl<T,E> {

  public readonly ok: false = false;

  /**
   * @param error The error held by the Result.
   */
  constructor(public readonly error: E) {}

  map<T2>(f: (val: T) => T2): Err<T2,E> {
    return err(this.error);
  }

  mapError<E2>(f: (error: E) => E2): Err<T,E2> {
    return err(f(this.error));
  }

  mapBoth<T2,E2>(f1: (val: T) => T2, f2: (error: E) => E2): Err<T2,E2> {
    return err(f2(this.error));
  }

  then<T2>(f1: (val: T) => Result<T2,E>, f2?: (error: E) => Result<T2,E>): Result<T2,E> {
    if (f2) return f2(this.error);
    return err(this.error);
  }

  try<T2>(f: (val: T) => T2, handleError: (e: unknown) => E): Result<T2,E> {
    return err(this.error);
  }

  recover<E2>(f: (error: E) => Result<T,E2>): Result<T,E2> {
    return f(this.error);
  }

  withDefault(val: T): T {
    return val;
  }

  catch(f: (error: E) => T): T {
    return f(this.error);
  }

  asPromise(): Promise<T> {
    return Promise.reject(this.error);
  }

}

/**
 * Create a Result from the given error.
 * @param error The error to hold in the Result.
 * @returns A new Result object.
 */
export function err<E>(error: E): Err<any,E> {
  return new Err(error);
}

/**
 * A Result holds either a value of type T or an error of type E. The `isOk`
 * property can be used to distinguish between the two cases:
 *
 * ```
 * function example<T,E>(res: Result<T,E>) {
 *   if (res.isOk) {
 *     // Can use res.value in this block.
 *   }
 *   else {
 *     // Can use res.error in this block.
 *   }
 * }
 * ```
 */
export type Result<T,E=string> = Ok<T,E> | Err<T,E>;

/**
 * Flatten a Result that contains another Result.
 * @param res A Result that may hold another Result.
 * @returns A new Result object.
 */
export function join<T,E>(res: Result<Result<T,E>,E>): Result<T,E> {
  if (res.ok) return res.value;
  return err(res.error);
}

/**
 * Transform a function that may throw into a function that returns a Result
 * without throwing.
 * @param f A function that may throw.
 * @param liftError A function that provides an error value if `f` throws.
 * @returns A function that takes the same input as `f` but returns a Result.
 */
export function lift<T,T2,E>(f: (val: T) => T2, liftError: (e: unknown) => E): (val: T) => Result<T2,E> {
  return val => {
    try {
      return ok(f(val));
    }
    catch(e) {
      return err(liftError(e));
    }
  }
}
