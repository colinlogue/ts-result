# TypeScript Result type

Defines a `Result` type for type-safe error handling,
similar to the same type in Elm or Rust.

## API

The following functions are exported from the module:

```typescript
/**
 * Create a Result from the given value.
 * @param val The value to hold in the Result.
 * @returns A new Result object.
 */
function ok<T>(val: T): Result<T,any>;

/**
 * Create a Result from the given error.
 * @param error The error to hold in the Result.
 * @returns A new Result object.
 */
function err<E>(error: E): Result<any,E>;

/**
 * Flatten a Result that contains another Result.
 * @param res A Result that may hold another Result.
 * @returns A new Result object.
 */
function join<T,E>(res: Result<Result<T,E>,E>): Result<T,E>;

/**
 * Transform a function that may throw into a function that returns a Result
 * without throwing.
 * @param f A function that may throw.
 * @param liftError A function that provides an error value if `f` throws.
 * @returns A function that takes the same input as `f` but returns a Result.
 */
function lift<T,T2,E>(f: (val: T) => T2, liftError: (e: unknown) => E): (val: T) => Result<T2,E>;
```

Note that the only way to create a value of type `Result` is with these functions,
as the underlying classes are not exposed.

Values of type `Result` have the following interface:

```typescript
/**
 * True when the Result holds a value, false when it holds an error.
 */
ok: boolean;

/**
 * Only present on the Ok variant, when the `ok` property is `true`.
 */
value: T;

/**
 * Only present on the Err variant, when the `ok` property is `false`.
 */
error: E;

/**
 * Transform the value using a given function, if the Result holds a value.
 * @param f The function to apply to the value.
 * @returns A new Result object.
 */
map<T2>(f: (val: T) => T2): Result<T2,E>;

/**
 * Transform the error using a given function, if the Result holds an error.
 * @param f The function to apply to the error.
 * @returns A new Result object.
 */
mapError<E2>(f: (error: E) => E2): Result<T,E2>;

/**
 * Transform the Result using one of the given functions depending on whether
 * the Result holds a value or an error.
 * @param f1 The function to apply if the Result holds a value.
 * @param f2 The function to apply if the Result holds an error.
 * @returns A new Result object.
 */
mapBoth<T2,E2>(f1: (val: T) => T2, f2: (error: E) => E2): Result<T2,E2>;

/**
 * Chain together multiple functions that return a Result.
 * @param f1 The function to apply if the Result holds a value.
 * @param f2 An optional function to apply if the Result holds an error.
 * @returns A new Result object.
 */
then<T2>(f1: (val: T) => Result<T2,E>, f2?: (error: E) => Result<T2,E>): Result<T2,E>;

/**
 * Recover from a potential error. Allows partial recovery by mapping error
 * values to either an Ok value or an Err that may optionally hold a different
 * error type.
 * @param f The function to apply if the Result holds an error.
 * @returns A new Result object.
 */
recover<E2>(f: (error: E) => Result<T,E2>): Result<T,E2>;

/**
 * Attempts to transform the value using the given function, if the Result
 * holds a value. If an error is thrown, the provided handler catches it and
 * transforms into the appropriate error type.
 * @param f A function that may throw.
 * @param handleError A function to handle the error if `f` throws.
 * @returns A new Result object.
 */
try<T2>(f: (val: T) => T2, handleError: (e: unknown) => E): Result<T2,E>;

/**
 * Get the value from the Result if it holds one, or a default value it not.
 * @param val The default value to use if the Result holds an error.
 */
withDefault(val: T): T;

/**
 * Get the value from the Result if it holds one, or transform the error into
 * a value if not.
 * @param f The function to apply if the Result holds an error.
 */
catch(f: (error: E) => T): T;

/**
 * Transform the Result into a Promise that either resolves to the value if it
 * holds one, or rejects with the error as the reason otherwise.
 */
asPromise(): Promise<T>;
```